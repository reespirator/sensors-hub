# Sensors Hub

Sensors Hub for Reespirator 2020 es un dispositivo diseñado por Alberto Ferrer (Sucro Tronic) que ha diseñado el circuito con su esquemático, la PCB y un firmware de test. Se trata de un hub de sensores que permite conectar sensores analógicos y digitales que utilizan diferentes protocolos como i2c, o SPI y generar mensajes CAN o RS485 para su envío al PLC o el Arduino que controla el Reespirator 2020. Su funcionalidad completa es mucho mayor de lo aquí descrito.
En la siguiente URL puedes ver el diseño electrónico: https://easyeda.com/Sucro-Tronic/reespirator2020-sensors 
En este repositorio publicaremos diferentes firmwares para poder utilizarlo con distintos sensores. 